Cryptoauthlib Standalone Test Project using SAMD21 Xplained Pro
=====================================================

Project:

This project implement basic testing for Atmel ECC508A device that communicate using I2C protocol with SAMD21 Xplained Pro board.
All drivers for this example are created by Atmel Software Framework (ASF).

#NOTE: this is initial project for the standalone test project that will be replicated to the SAML22 Xplained Pro B board project

Prerequisites for this example
--------------------------------
Software:
  - Atmel Studio 7
  
Hardware:
  - Atmel SAMD21 Xplained Pro
  - Atmel AT88CKSCKTSOIC extension board


Building the Example Source Code
----------------------------------
If you are using Atmel Studio 7, then you should load the project file:
  - USART_QUICK_START1.atsln

Once the project has been compiled, you can flash it to the SAMD21 using the Debugging button in the Debug toolbar
or press F5 to instant access.


Using the Example Project
--------------------------

Connect your host computer to the EDBG USB to program and/or debug it.  

Use a terminal program on your host and connect it to the SAMD21 Xplained Pro EDBG USB. 
This particular step will vary on each computer and operating system.

The communication parameters are:
  - 115,200 baud
  - 8 bit word
  - No parity
  - 1 stop bit
 
Once you've connected to the terminal serial usb, the main application will begin to discover the ECC508A device. After the device has been detected, press the SW0
button to initiate the test procedure. From the Serial Terminal, it will be displayed test status of the connected device.

Progress Log:
--------------
20170228: add basic testing (read DevRev, read S/N, write Configuration Zone, return Defaul Configuration Zone) using CyptoAuthLib in SAMD21 Xplained Pro board. 
Debugging process is done by connect SAMD21 EDBG USB to the Host Computer USB

20170301: add device discovery before test procedure start. add SW0 as a trigger to start the test procedure.