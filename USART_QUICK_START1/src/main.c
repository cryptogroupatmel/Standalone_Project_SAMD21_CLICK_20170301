/**
 * \file
 *
 * \brief SAM USART Quick Start
 *
 * Copyright (C) 2012-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include "asf.h"
// #include <ctype.h>
// #include <stdio.h>
// #include <string.h>
// #include <board.h>
#include "cryptoauthlib.h"
#include "test/atca_unit_tests.h"
#include "test/atca_basic_tests.h"
#include "test/atca_crypto_sw_tests.h"
#include "host/atca_host.h"
#include "stdio_serial.h"

void configure_usart(void);

struct usart_module usart_instance;

void configure_usart(void)
{
	struct usart_config config_usart;

	usart_get_config_defaults(&config_usart);
	config_usart.baudrate    = 115200;
	config_usart.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	config_usart.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	config_usart.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	config_usart.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	config_usart.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;

	while (usart_init(&usart_instance,EDBG_CDC_MODULE, &config_usart) != STATUS_OK) {
	}
	usart_enable(&usart_instance);
}

int main(void)
{
	system_init();

	configure_usart();

	delay_init();

	uint16_t temp;
	ATCA_STATUS status = !ATCA_SUCCESS;
	
	// set default device configuration
	gCfg = &cfg_ateccx08a_i2c_default;


	// discover device
	uint8_t revision[4];

	atcab_init( gCfg );
	status =  atcab_info( revision );
	atcab_release();

	if (status == ATCA_SUCCESS)
	{
		uint8_t str_detected[] = "\rECC508A detected!";
		usart_write_buffer_wait(&usart_instance, str_detected, sizeof(str_detected));
	}
	else
	{
		uint8_t str_detected[] = "\rECC508A not detected!";
		usart_write_buffer_wait(&usart_instance, str_detected, sizeof(str_detected));
	}
	// end discover device
	
	while (true) {
	
		// if button is pressed
		if (port_pin_get_input_level(BUTTON_0_PIN) == BUTTON_0_ACTIVE)
		{
			port_pin_set_output_level(LED_0_PIN, LED_0_ACTIVE);

			// test status
			bool test_pass = true;

			// Read DevRev
			char displaystr[15];
			int displaylen = sizeof(displaystr);			
			if ( status == ATCA_SUCCESS )
			{
				// check ECC508A device
				if ( revision[2] == 0x50 )
				{
					uint8_t str_info[] = "\r\n\nRead DevRev...\r\n";
					usart_write_buffer_wait(&usart_instance, str_info, sizeof(str_info));
					uint8_t str_revnum[] = "Revision Number: ";
					usart_write_buffer_wait(&usart_instance, str_revnum, sizeof(str_revnum));
					atcab_bin2hex(revision, 4, displaystr, &displaylen );
					usart_write_buffer_wait(&usart_instance, displaystr, sizeof(displaystr));
					uint8_t str_space[] = "\r\n";
					usart_write_buffer_wait(&usart_instance, str_space, sizeof(str_space));
				}
				else
				{
					uint8_t str_detected[] = "ECC508A not detected!\r\n";
					usart_write_buffer_wait(&usart_instance, str_detected, sizeof(str_detected));
				}
			}
			else
			{
				uint8_t str_detected[] = "ECC508A not detected!\r\n";
				usart_write_buffer_wait(&usart_instance, str_detected, sizeof(str_detected));
			}
			// End Read DevRev

			delay_ms (500);

			// if device is detected
			if (status == ATCA_SUCCESS)
			{				
				// Read Device s/n
				uint8_t sernum[9];
				char displaySN[30];
				int displaySNlen = sizeof(displaySN);

				uint8_t str_SN[] = "\r\nRead Serial Number...\r\n";
				usart_write_buffer_wait(&usart_instance, str_SN, sizeof(str_SN));

				atcab_init( gCfg );
				status = atcab_read_serial_number( sernum );
				atcab_release();

				if (status == ATCA_SUCCESS)
				{
					uint8_t str_sernum[] = "Serial Number: ";
					usart_write_buffer_wait(&usart_instance, str_sernum, sizeof(str_sernum));
					atcab_bin2hex(sernum, 9, displaySN, &displaySNlen );
					usart_write_buffer_wait(&usart_instance, displaySN, sizeof(displaySN));
					uint8_t str_space[] = "\r\n";
					usart_write_buffer_wait(&usart_instance, str_space, sizeof(str_space));
				}
				else
					test_pass = false;
				// End Read Device s/n

				// Write ConfigZone Counter[1]
				bool isLocked = 0;
				uint8_t read_data[8];
				uint8_t default_data[8];
				char displayRead[30];
				int displayReadlen = sizeof(displayRead);
				uint8_t data_input[] = {0xEE, 0xEE, 0xEE, 0xEE, 0x00, 0x00, 0x00, 0x00};
				
				uint8_t str_writeConfig[] = "\r\nWrite ConfigZone...\r\n";
				usart_write_buffer_wait(&usart_instance, str_writeConfig, sizeof(str_writeConfig));

				atcab_init( gCfg );
				atcab_is_locked( LOCK_ZONE_CONFIG, &isLocked);
				if (isLocked == false)
				{
					atcab_read_bytes_zone(ATECC508A, ATCA_ZONE_CONFIG, 60, 8, read_data);
					atcab_bin2hex(read_data, 8, displayRead, &displayReadlen );
					usart_write_buffer_wait(&usart_instance, displayRead, sizeof(displayRead));

					// save default config zone data
					memcpy(default_data, read_data, sizeof(read_data));

					status = atcab_write_bytes_zone(ATECC508A, ATCA_ZONE_CONFIG, 60, &data_input, sizeof(data_input));

					if (status == ATCA_SUCCESS)
					{
						uint8_t str_successWrite[] = "\r\nSuccess Write to ConfigZone 60\r\n";
						usart_write_buffer_wait(&usart_instance, str_successWrite, sizeof(str_successWrite));

						atcab_read_bytes_zone(ATECC508A, ATCA_ZONE_CONFIG, 60, 8, read_data);
						atcab_bin2hex(read_data, 8, displayRead, &displayReadlen );
						usart_write_buffer_wait(&usart_instance, displayRead, sizeof(displayRead));
						uint8_t str_space[] = "\r\n";
						usart_write_buffer_wait(&usart_instance, str_space, sizeof(str_space));
					}
					else
					{
						uint8_t str_failWrite[] = "\r\nFailed to Write ConfigZone\r\n";
						usart_write_buffer_wait(&usart_instance, str_failWrite, sizeof(str_failWrite));
						test_pass = false;
					}
				}
				else
				{
					uint8_t str_failWrite[] = "\r\nConfigZone must be unlocked\r\n";
					usart_write_buffer_wait(&usart_instance, str_failWrite, sizeof(str_failWrite));
					test_pass = false;
				}
				atcab_release();
				// End Write ConfigZone Counter[1]
			
				// Return Default ConfigZone
				uint8_t str_defaultConfig[] = "\r\nReturn ConfigZone Default Value...\r\n";
				usart_write_buffer_wait(&usart_instance, str_defaultConfig, sizeof(str_defaultConfig));

				atcab_init( gCfg );
				atcab_is_locked( LOCK_ZONE_CONFIG, &isLocked);
				if (isLocked == false)
				{
					atcab_read_bytes_zone(ATECC508A, ATCA_ZONE_CONFIG, 60, 8, read_data);
					atcab_bin2hex(read_data, 8, displayRead, &displayReadlen );
					usart_write_buffer_wait(&usart_instance, displayRead, sizeof(displayRead));

					status = atcab_write_bytes_zone(ATECC508A, ATCA_ZONE_CONFIG, 60, &default_data, sizeof(default_data));

					if (status == ATCA_SUCCESS)
					{
						uint8_t str_successReturn[] = "\r\nConfigZone 60 Default Value\r\n";
						usart_write_buffer_wait(&usart_instance, str_successReturn, sizeof(str_successReturn));

						atcab_read_bytes_zone(ATECC508A, ATCA_ZONE_CONFIG, 60, 8, read_data);
						atcab_bin2hex(read_data, 8, displayRead, &displayReadlen );
						usart_write_buffer_wait(&usart_instance, displayRead, sizeof(displayRead));
						uint8_t str_space[] = "\r\n";
						usart_write_buffer_wait(&usart_instance, str_space, sizeof(str_space));
					}
					else
					{
						uint8_t str_failReturn[] = "\r\nFailed to Restore ConfigZone\r\n";
						usart_write_buffer_wait(&usart_instance, str_failReturn, sizeof(str_failReturn));
						test_pass = false;
					}
				}
				else
				{
					uint8_t str_failReturn[] = "\r\nConfigZone must be unlocked\r\n";
					usart_write_buffer_wait(&usart_instance, str_failReturn, sizeof(str_failReturn));
					test_pass = false;
				}
				atcab_release();
				// End Return Default ConfigZone

				if (test_pass == true)
				{
					uint8_t str_redult[] = "\r\nPASS\r\n";
					usart_write_buffer_wait(&usart_instance, str_redult, sizeof(str_redult));
				}
				else
				{
					uint8_t str_redult[] = "\r\nFAILED\r\n";
					usart_write_buffer_wait(&usart_instance, str_redult, sizeof(str_redult));
				}
			}

			// if device is not detected
			else
			{
				port_pin_set_output_level(LED_0_PIN, !LED_0_ACTIVE);
			}
		}

		// if button isnot pressed
		else
		{
			
		}
	}

}
